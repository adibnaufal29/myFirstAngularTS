import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

@Component({
  selector: 'app-data-mahasiswa',
  templateUrl: './data-mahasiswa.component.html',
  styleUrls: ['./data-mahasiswa.component.css']
})
export class DataMahasiswaComponent implements OnInit {

  constructor(private http: Http) { }
  confirmationString:string = "Data Telah ditambahkan!";
  isAdded: boolean = false;
  DataMahasiswaObj:object = {};

  tambahDataMhs = function(data_mhs) {
    this.DataMahasiswaObj = {
      //"id" : data_mhs.id_mhs,
      "nim" : data_mhs.nim,
      "nama_mhs" : data_mhs.nama_mhs,
      "alamat_mhs" : data_mhs.alamat_mhs
    }
    this.http.post("http://localhost:3000/data_mahasiswa/", this.DataMahasiswaObj).subscribe((res:Response) =>{
      //console.log(res);
      this.isAdded = true;
    })
  }

  ngOnInit() {
  }

}
