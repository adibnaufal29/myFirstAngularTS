import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-update-mahasiswa',
  templateUrl: './update-mahasiswa.component.html',
  styleUrls: ['./update-mahasiswa.component.css']
})
export class UpdateMahasiswaComponent implements OnInit {

  id:number;
  data:object = [];
  data_mahasiswa = [];
  exist = false;
  DataMahasiswaObj = {};
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private router: Router, private route: ActivatedRoute, private http: Http  ) { }

  updateData(DataMahasiswa) {
      this.DataMahasiswaObj = {
        "nim" : DataMahasiswa.nim,
        "nama_mhs" : DataMahasiswa.nama_mhs,
        "alamat_mhs": DataMahasiswa.alamat_mhs
      };
      const url = `${"http://localhost:3000/data_mahasiswa"}/${this.id}`;
      this.http.put(url, JSON.stringify(this.DataMahasiswaObj), {headers: this.headers})
        .toPromise()
        .then(() => {
        this.router.navigate(['/']);
      })
  }

  ngOnInit() {
      this.route.params.subscribe(params => {
      this.id = +params['id'];
      });
      this.http.get("http://localhost:3000/data_mahasiswa").subscribe(
        (res: Response) => {
          this.data_mahasiswa = res.json();
          for(var i = 0; i < this.data_mahasiswa.length ; i++) {
            if(parseInt(this.data_mahasiswa[i].id) === this.id) {
              this.exist = true;
              this.data = this.data_mahasiswa[i];
              break;
            } else {
              this.exist = false;
            }
          }
        }
      )
    }
  }
