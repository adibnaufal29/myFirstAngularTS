import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NewDataComComponent } from './new-data-com/new-data-com.component';
import { Http } from '@angular/http';
import { DataMahasiswaComponent } from './data-mahasiswa/data-mahasiswa.component';
import { UpdateMahasiswaComponent } from './update-mahasiswa/update-mahasiswa.component';


//const appRoutes: Routes = [
  //{ path: 'lihat_data', component: LihatData }
  //{ path: '**', component: PageNotFoundComponent }
//];

@NgModule({
  declarations: [
    AppComponent,
    NewDataComComponent,
    DataMahasiswaComponent,
    UpdateMahasiswaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      {path: "",component: NewDataComComponent},
      {path: "tambah_data",component: DataMahasiswaComponent},
      {path: "updateData/:id",component: UpdateMahasiswaComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})



export class AppModule { }
