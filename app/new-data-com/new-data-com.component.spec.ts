import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewDataComComponent } from './new-data-com.component';

describe('NewDataComComponent', () => {
  let component: NewDataComComponent;
  let fixture: ComponentFixture<NewDataComComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewDataComComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewDataComComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
