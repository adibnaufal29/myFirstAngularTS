import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-new-data-com',
  templateUrl: './new-data-com.component.html',
  styleUrls: ['./new-data-com.component.css']
})
export class NewDataComComponent implements OnInit {

  constructor(private http: Http) { }
  id:number;
  private headers = new Headers({'Content-Type': 'application/json'});

  data_mahasiswa =[];
  fetchData = function(){
    this.http.get("http://localhost:3000/data_mahasiswa").subscribe(
      (res: Response) => {
        this.data_mahasiswa = res.json();
      }
    )
  }

  deleteMahasiswa = function(id){
    if(confirm("Hapus data?")) {
      const url = `${"http://localhost:3000/data_mahasiswa"}/${id}`;
      return this.http.delete(url, {headers: this.headers}).toPromise()
      .then(() =>{
        this.fetchData();
      })
    }
  }

  ngOnInit() {
    this.fetchData();
  }

}
